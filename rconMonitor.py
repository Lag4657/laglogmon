#!/usr/bin/python3

import time
import re
import json
import requests



serverNames = list()
logs = list()
logDirs = list()
try:
    gameini = r'/home/steam/pavlovserver/Pavlov/Saved/Config/LinuxServer/Game.ini'
    gameini = open(gameini,'r')
    pattern = '\nServerName=(.*)'

    serverNames.append(re.findall(pattern,gameini.read())[0])
    logs.append(open(r'/home/steam/pavlovserver/Pavlov/Saved/Logs/Pavlov.log','r'))
except:
    for i in range(1,10):
        try:
            gameini = rf'/home/steam/pavlovserver{i}/Pavlov/Saved/Config/LinuxServer/Game.ini'
            gameini = open(gameini,'r')
            pattern = '\nServerName=(.*)'
            serverNames.append(re.findall(pattern,gameini.read())[0])
            logs.append(open(rf'/home/steam/pavlovserver{i}/Pavlov/Saved/Logs/Pavlov.log','r'))
            logDirs.append(rf'/home/steam/pavlovserver{i}/Pavlov/Saved/Logs/Pavlov.log')
        except Exception as e:
            print(e,flush=True)
    if serverNames == []:
        print('Cant find pavlovserver',flush=True)
        exit()
    


for i in range(len(logs)):

    logs[i].read()
print(serverNames,flush=True)
blankCounters = [0]*len(logs)
while True:
    time.sleep(5)
    for i in range(len(logs)):
        newLogs = logs[i].read() + '\n'
        if newLogs != '\n':
            blankCounters[i] = 0
        #################################################### RCON COMMAND SEARCH ###################################################
            pattern = '((Rcon): (\S+)[ ]?(.*))|((Rcon Plus) Command Executed: (.+?) (.*))\nSuccessful: (\S*)\s*Instigated By: (.*)'
            commands = re.findall(pattern,newLogs)
            
            for command in commands:
                if command[2] not in ['Client','User','RefreshList','InspectPlayer','SwitchTeam','ResetSND','Connection']:
                    if command[4] == '':
                        commandJSON = {command[1]:{'command':command[2],
                                                'values':command[3]}}
                        commandMsg = command[0]
                    else:
                        commandJSON = {command[5]:{'Command':command[6],
                                                'Values':command[7],
                                                'Successful':bool(command[8]),
                                                'Instigated By':command[9]}}
                        commandMsg = command[4]
                    
                    message = f'{serverNames[i]}:\n{commandJSON}'
                    data = {'content':message}
                    requests.post('https://discord.com/api/webhooks/1191726492934217828/XlHLM4VtqsprZpAmb2qKtt3DhWiee52dPmwqHzZ8bLFLGm9sAsnXkfDzzHBOmkSnNj6Q',json=data)
        ############################################################################################################################
        else:
           
            blankCounters[i] += 1
            if blankCounters[i] > 60:
                logs[i] = open(logDirs[i])
                blankCounters[i] = 0
        

