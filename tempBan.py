import time
import re
import asyncio
import pavlov

async def ban(name):
    rcon = pavlov.PavlovRCON('localhost',9100,'password')
    rcon.send(f'Ban {name}')
    rcon.close()

async def unban(name):
    rcon = pavlov.PavlovRCON('localhost',9100,'password')
    rcon.send(f'Unban {name}')
    rcon.close()

serverNames = list()
logs = list()
logDirs = list()
tempList = dict()

try:
    gameini = r'/home/steam/pavlovserver/Pavlov/Saved/Config/LinuxServer/Game.ini'
    gameini = open(gameini,'r')
    pattern = '\nServerName=(.*)'

    serverNames.append(re.findall(pattern,gameini.read())[0])
    logs.append(open(r'/home/steam/pavlovserver/Pavlov/Saved/Logs/Pavlov.log','r'))
except:
    for i in range(1,10):
        try:
            gameini = rf'/home/steam/pavlovserver{i}/Pavlov/Saved/Config/LinuxServer/Game.ini'
            gameini = open(gameini,'r')
            pattern = '\nServerName=(.*)'
            serverNames.append(re.findall(pattern,gameini.read())[0])
            logs.append(open(rf'/home/steam/pavlovserver{i}/Pavlov/Saved/Logs/Pavlov.log','r'))
            logDirs.append(rf'/home/steam/pavlovserver{i}/Pavlov/Saved/Logs/Pavlov.log')
        except Exception as e:
            print(e,flush=True)
    if serverNames == []:
        print('Cant find pavlovserver',flush=True)
        exit()
    

counter = 0
for i in range(len(logs)):

    logs[i].read()
print(serverNames,flush=True)
blankCounters = [0]*len(logs)
while True:
    time.sleep(5)

    counter += 1

    if counter == 720:
        counter = 0
        for player in tempList:
            tempList[player] -= 1
            if tempList[player] == 0:
                asyncio.run(unban(player))
                tempList.pop(player)

    for i in range(len(logs)):
        newLogs = logs[i].read() + '\n'
        if newLogs != '\n':
            blankCounters[i] = 0
        #################################################### RCON COMMAND SEARCH ###################################################
            pattern = '((Rcon): (\S+)[ ]?(.*))|((Rcon Plus) Command Executed: (.+?) (.*))\nSuccessful: (\S*)\s*Instigated By: (.*)'
            commands = re.findall(pattern,newLogs)
            
            for command in commands:
                if command[2] not in ['Client','User','RefreshList','InspectPlayer','SwitchTeam','ResetSND','Connection']:
                    commandJSON:dict[str:str]
                    if command[4] == '':
                        commandJSON = {command[1]:{'Command':command[2],
                                                'Values':command[3]}}
                        
                    else:
                        commandJSON = {command[5]:{'Command':command[6],
                                                'Values':command[7],
                                                'Successful':bool(command[8]),
                                                'Instigated By':command[9]}}
                       
                    
                    
                    if commandJSON['Command'].lower() == 'ban':
                        values:str = commandJSON['Values']
                        values = values.strip().split(' ')
                        if len(values) > 1:
                            asyncio.run(ban(values[0]))
                            tempList.update({values[0]:values[1]})



        ############################################################################################################################
        else:
           
            blankCounters[i] += 1
            if blankCounters[i] > 60:
                logs[i] = open(logDirs[i])
                blankCounters[i] = 0
        

