import time
import re
import json
import requests



serverNames = list()
logs = list()
logDirs = list()
try:
    gameini = r'/home/steam/pavlovserver/Pavlov/Saved/Config/LinuxServer/Game.ini'
    gameini = open(gameini,'r')
    pattern = '\nServerName=(.*)'

    serverNames.append(re.findall(pattern,gameini.read())[0])
    logs.append(open(r'/home/steam/pavlovserver/Pavlov/Saved/Logs/Pavlov.log','r'))
except:
    for i in range(1,10):
        try:
            gameini = rf'/home/steam/pavlovserver{i}/Pavlov/Saved/Config/LinuxServer/Game.ini'
            gameini = open(gameini,'r')
            pattern = '\nServerName=(.*)'
            serverNames.append(re.findall(pattern,gameini.read())[0])
            logs.append(open(rf'/home/steam/pavlovserver{i}/Pavlov/Saved/Logs/Pavlov.log','r'))
            logDirs.append(rf'/home/steam/pavlovserver{i}/Pavlov/Saved/Logs/Pavlov.log')
        except Exception as e:
            print(e,flush=True)
    if serverNames == []:
        print('Cant find pavlovserver',flush=True)
        exit()
    
for log in logs:
    log.read()
blankCounters = [0]*len(logs)
while True:
    time.sleep(5)
    for i in range(len(logs)):
        newLogs = logs[i].read() + '\n'
        if newLogs != '\n':
            blankCounters[i] = 0
        ##################################################### JSON SEARCH #########################################################                                       
            pattern = '(\[.*?\]).*StatManagerLog: ({[\s\S]*?\n})\n'
            stats = re.findall(pattern,newLogs)                
            for stat in stats:                          
                try:                                      
                    logJSON = json.loads(stat[1])
                    print(stat[0],': ',logJSON)
                    
                except Exception as e:
                    print(e)
                    print(stat)
                    exit()             
        ###########################################################################################################################
            
        ################################################### PLAYER JOIN SEARCH ####################################################
            pattern = '(\[.*?\]).*Player (\w*?) Joined[\s\S]*?Join succeeded: (.+?)\n'
            joined = re.findall(pattern, newLogs)
            for user in joined: 
                joinJSON = {'playerJoin':{'oculusName':user[2],'eosId':user[1]}}
                
                print(user[0],': ',joinJSON)
        ############################################################################################################################ 
            
        #################################################### RCON COMMAND SEARCH ###################################################
            pattern = '(\[.*?\]).*((Rcon): (\S+)[ ]?(.*))|((Rcon Plus) Command Executed: (.+?) (.*))'
            commands = re.findall(pattern,newLogs)
            
            for command in commands:
                if command[3] not in ['Client','User','RefreshList','InspectPlayer','SwitchTeam','ResetSND','Connection']:
                    if command[5] == '':
                        commandJSON = {command[2]:{'command':command[3],
                                                'values':command[4]}}
                        commandMsg = command[1]
                    else:
                        commandJSON = {command[6]:{'command':command[7],
                                                'values':command[8]}}
                        commandMsg = command[5]
                    print(command[0],': ',commandMsg,flush=True)
                    message = f'{serverNames[i]}:\n{commandMsg}'
                    data = {'content':message}
                    requests.post('https://discord.com/api/webhooks/1191726492934217828/XlHLM4VtqsprZpAmb2qKtt3DhWiee52dPmwqHzZ8bLFLGm9sAsnXkfDzzHBOmkSnNj6Q',json=data)
        ############################################################################################################################
        else:
            blankCounters[i] += 1
            if blankCounters[i] > 30:
                logs[i] = open(logDirs[i])
                blankCounters[i] = 0
            
    

