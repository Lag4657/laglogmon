import time
import re
import json

import asyncio
import pavlov

async def onRoundStart():
    rcon = pavlov.PavlovRCON('localhost',9100,'password')
    rcon.send('<put command here>')
    rcon.send('<next command>')
    rcon.send('for RCON+, theres no response, so you put False',False)



serverNames = list()
logs = list()
logDirs = list()
try:
    gameini = r'/home/steam/pavlovserver/Pavlov/Saved/Config/LinuxServer/Game.ini'
    gameini = open(gameini,'r')
    pattern = '\nServerName=(.*)'

    serverNames.append(re.findall(pattern,gameini.read())[0])
    logs.append(open(r'/home/steam/pavlovserver/Pavlov/Saved/Logs/Pavlov.log','r'))
except:
    for i in range(1,10):
        try:
            gameini = rf'/home/steam/pavlovserver{i}/Pavlov/Saved/Config/LinuxServer/Game.ini'
            gameini = open(gameini,'r')
            pattern = '\nServerName=(.*)'
            serverNames.append(re.findall(pattern,gameini.read())[0])
            logs.append(open(rf'/home/steam/pavlovserver{i}/Pavlov/Saved/Logs/Pavlov.log','r'))
            logDirs.append(rf'/home/steam/pavlovserver{i}/Pavlov/Saved/Logs/Pavlov.log')
        except Exception as e:
            print(e,flush=True)
    if serverNames == []:
        print('Cant find pavlovserver',flush=True)
        exit()
    
for log in logs:
    log.read()
blankCounters = [0]*len(logs)
while True:
    time.sleep(5)
    for i in range(len(logs)):
        newLogs = logs[i].read() + '\n'
        if newLogs != '\n':
            blankCounters[i] = 0
        ##################################################### JSON SEARCH #########################################################                                       
            pattern = 'StatManagerLog: {[\s\S]*?\n}\n'
            search = re.findall(pattern,newLogs)                
            for match in search:                          
                try:                                      
                    logJSON:dict = json.loads(match[16:])
                    roundLog:dict = logJSON.get('RoundState')
                    if roundLog:
                        if roundLog.get('State') == 'Started':
                            asyncio.run(onRoundStart())

                    
                except Exception as e:
                    print(e)
                    print(match)
                    exit()             
        ###########################################################################################################################
            
        
        else:
            blankCounters[i] += 1
            if blankCounters[i] > 30:
                logs[i] = open(logDirs[i])
                blankCounters[i] = 0
            
    

