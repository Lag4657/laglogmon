import time
import re
import asyncio
from pavlov import PavlovRCON


async def giveMenu(player):
    try:
        rcon = PavlovRCON(ip,port,password)
        await rcon.send(f'Notify {player} Granting RCON+ Menu',False)
        await rcon.send(f'GiveMenu {player}',False)
        await rcon.close()
        return True
    except:
        return False

rconInfo = open(r'/home/steam/pavlovserver/Pavlov/Saved/Config/RconSettings.txt').read()
pattern = 'Password=(.*)'
password = re.findall(pattern,rconInfo)[0]
pattern = 'Port=(.*)'
port = int(re.findall(pattern,rconInfo)[0])

ip = '127.0.0.1'



serverNames = list()
logs = list()
logDirs = list()

log = open(r'/home/steam/pavlovserver/Pavlov/Saved/Logs/Pavlov.log')
log.read()
blankCounter = 0

while True:
    time.sleep(5)
    
    newLogs = log.read() + '\n'
    if newLogs != '\n':
        blankCounter = 0

        ######################### Player Join Search ############################
        pattern = 'Player (\w*?) Joined[\s\S]*?Join succeeded: (.+?)\n'
        joined = re.findall(pattern, newLogs)
        for user in joined: 
            modList = open(r'/home/steam/pavlovserver/Pavlov/Saved/Config/mods.txt').read()
            if user[1] in modList:
                working = asyncio.run(giveMenu(user[1]))
            if not working:
                break

        
    else:
        blankCounter += 1
        if blankCounter > 30:
            log = open(r'/home/steam/pavlovserver/Pavlov/Saved/Logs/Pavlov.log')
            blankCounter = 0